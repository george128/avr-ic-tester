/*
 * keyboard.h
 *
 * Created: 24.07.2015 19:55:45
 *  Author: Trol
 */ 

#include "memory_test256k.h"

#include <util/delay.h>
#include <avr/io.h>

#include "debug.h"
#include "tester_io.h"

/*********************************************************************************************************
         _______  _______
   L6  --| 1    \/   20 |-- L7
   L5  --| 2         19 |-- C0
   L4  --| 3         18 |-- C1
   L3  --| 4         17 |-- C2
   L2  --| 5         16 |-- C3
   A7  --| 6         15 |-- C4
   A6  --| 7         14 |-- C5
   A5  --| 8         13 |-- C6
   A4  --| 9         12 |-- C7
   A0  --| 10        11 |-- B6
         ----------------


	           __________________
	(A7)  6  --| A0  |     |    |
	(A6)  7  --| A1  |     |    |
	(A5)  8  --| A2  |     |    |
	(A4)  9  --| A3  |     | D0 |--  1  (L6)
	(B6) 11  --| A4  |     | D1 |--  2  (L5)
	(C7) 12  --| A5  |     | D2 |-- 18  (C1)
	(C6) 13  --| A6  |     | D3 |-- 19  (C0)
	(C5) 14  --| A7  |     |    |
	(C4) 15  --| A8  |     |    |
	           |-----|     |----|
	(L3)  4  --|~RAS |     |    |
	(C2) 17  --|~CAS |     | 5V |-- 10  (A0) !!!
	           |-----|     |    |
	(L4)  3  --|~WE  |     | GND|-- 20  (L7) !!!
	(C3) 16  --|~OE  |     |    |
	           ------------------

*********************************************************************************************************/

#define VCC _BV(PINA0)
#define PORT_VCC PORTA
#define DIR_VCC DDRA

// unused for now
#define VCCP _BV(PINB7)
#define PORT_VCCP PORTB
#define DIR_VCCP DDRB

#define GND _BV(PINL7)
#define PORT_GND PORTL
#define DIR_GND DDRL

#define A0 _BV(PINA7)
#define PORT_A0 PORTA
#define DIR_A0 DDRA

#define A1 _BV(PINA6)
#define PORT_A1 PORTA
#define DIR_A1 DDRA

#define A2 _BV(PINA5)
#define PORT_A2 PORTA
#define DIR_A2 DDRA

#define A3 _BV(PINA4)
#define PORT_A3 PORTA
#define DIR_A3 DDRA

#define A4 _BV(PINB6)
#define PORT_A4 PORTB
#define DIR_A4 DDRB

#define A5 _BV(PINC7)
#define PORT_A5 PORTC
#define DIR_A5 DDRC

#define A6 _BV(PINC6)
#define PORT_A6 PORTC
#define DIR_A6 DDRC

#define A7 _BV(PINC5)
#define PORT_A7 PORTC
#define DIR_A7 DDRC

#define A8 _BV(PINC4)
#define PORT_A8 PORTC
#define DIR_A8 DDRC

#define WE _BV(PINL4)
#define PORT_WE PORTL
#define DIR_WE DDRL

#define OE _BV(PINC3)
#define PORT_OE PORTC
#define DIR_OE DDRC

#define RAS _BV(PINL3)
#define PORT_RAS PORTL
#define DIR_RAS DDRL

#define CAS _BV(PINC2)
#define PORT_CAS PORTC
#define DIR_CAS DDRC

#define DQ0 _BV(PINL6)
#define PORT_DQ0 PORTL
#define DIR_DQ0 DDRL
#define IN_DQ0 PINL

#define DQ1 _BV(PINL5)
#define PORT_DQ1 PORTL
#define DIR_DQ1 DDRL
#define IN_DQ1 PINL

#define DQ2 _BV(PINC1)
#define PORT_DQ2 PORTC
#define DIR_DQ2 DDRC
#define IN_DQ2 PINC

#define DQ3 _BV(PINC0)
#define PORT_DQ3 PORTC
#define DIR_DQ3 DDRC
#define IN_DQ3 PINC

#define rows 9

#define set_RAS()	PORT_RAS |= RAS
#define clr_RAS()	PORT_RAS &= ~RAS
#define set_CAS()	PORT_CAS |= CAS
#define clr_CAS()	PORT_CAS &= ~CAS
#define set_WE()	PORT_WE |= WE
#define clr_WE()	PORT_WE &= ~WE
#define set_OE()	PORT_OE |= OE
#define clr_OE()	PORT_OE &= ~OE


//#define nop() __asm__ volatile("nop;\n\tnop")
#define nop()

static uint16_t failures[8*8];
static uint8_t no_chip;

void Mem256kInit() {
	// Inputs
	DIR_DQ0 &=~DQ0;
	DIR_DQ1 &=~DQ1;
	DIR_DQ2 &=~DQ2;
	DIR_DQ3 &=~DQ3;

	//Outputs
	DIR_VCC |=VCC;
	DIR_VCCP |=VCCP;
	DIR_GND |=GND;

	DIR_RAS |=RAS;
	DIR_CAS |=CAS;
	DIR_WE  |=WE;
	DIR_OE  |=OE;

	DIR_A0 |=A0;
	DIR_A1 |=A1;
	DIR_A2 |=A2;
	DIR_A3 |=A3;
	DIR_A4 |=A4;
	DIR_A5 |=A5;
	DIR_A6 |=A6;
	DIR_A7 |=A7;
	DIR_A8 |=A8;


	PORT_VCC |= VCC;
	PORT_VCCP |= VCCP; //no strong pullup
	PORT_GND &= ~GND;

	PORT_DQ0 |= DQ0;
	PORT_DQ1 |= DQ1;
	PORT_DQ2 |= DQ2;
	PORT_DQ3 |= DQ3;

	PORT_A0 |=A0;
	PORT_A1 |=A1;
	PORT_A2 |=A2;
	PORT_A3 |=A3;
	PORT_A4 |=A4;
	PORT_A5 |=A5;
	PORT_A6 |=A6;
	PORT_A7 |=A7;
	PORT_A8 |=A8;

    set_RAS();
    set_CAS();
    set_OE();
    set_WE();

  for (int i = 0; i < 9; i++) {
    clr_RAS();
    nop();
    set_RAS();
    nop();
  }


}

static void set_data_out() {
	DIR_DQ0 |=DQ0;
	DIR_DQ1 |=DQ1;
	DIR_DQ2 |=DQ2;
	DIR_DQ3 |=DQ3;
}

static void set_data_in() {
	DIR_DQ0 &=~DQ0;
	DIR_DQ1 &=~DQ1;
	DIR_DQ2 &=~DQ2;
	DIR_DQ3 &=~DQ3;
}

#define set_data_bit(val, bit) if(val & _BV(bit))PORT_DQ##bit |= DQ##bit;else PORT_DQ##bit &= ~DQ##bit

static void set_data256k(uint8_t data) {
    set_data_bit(data, 0);
    set_data_bit(data, 1);
    set_data_bit(data, 2);
    set_data_bit(data, 3);
}

static uint8_t get_data256k() {
    uint8_t data;

    data  = IN_DQ0 & DQ0 ? _BV(0):0;
    data |= IN_DQ1 & DQ1 ? _BV(1):0;
    data |= IN_DQ2 & DQ2 ? _BV(2):0;
    data |= IN_DQ3 & DQ3 ? _BV(3):0;

    return data;
}

#define set_addr_bit(val, bit) if(val & _BV(bit))PORT_A##bit |= A##bit;else PORT_A##bit &= ~A##bit

static void setAddress256k(uint16_t val) {
    set_addr_bit(val, 0);
    set_addr_bit(val, 1);
    set_addr_bit(val, 2);
    set_addr_bit(val, 3);
    set_addr_bit(val, 4);
    set_addr_bit(val, 5);
    set_addr_bit(val, 6);
    set_addr_bit(val, 7);
    set_addr_bit(val, 8);
}

/************************************************************************/
/* ������ ������ ������                                                 */
/************************************************************************/
uint8_t Mem256kReadData(uint16_t row, uint16_t col) {
	set_RAS();
	set_CAS();
	set_WE();
	set_OE();

	setAddress256k(row);
	clr_RAS();

	setAddress256k(col);
	clr_CAS();

	clr_OE();

	// ��������� �������� ������ ��������� ��� ����� ������� ��������� ��� (��������� ���� �� ���������� ����������)
	uint8_t result = get_data256k();
	for (uint8_t i = 0; i < 3; i++) {
                uint8_t data;
		if ((data = get_data256k()) != result) {
			MSG_DEC("data ", result);
			MSG_DEC("Err data ", data);
			result = 0xff;
			break;
		}
	}

	set_OE();
	set_RAS();
	set_CAS();
	set_WE();

	return result;
}


/************************************************************************/
/* ������ ������ ������                                                 */
/************************************************************************/
void Mem256kWriteData(uint16_t row, uint16_t col, uint8_t val) {
	set_RAS();
	set_CAS();

	setAddress256k(row);
	clr_RAS();

	set_data_out();
	set_data256k(val);

	clr_WE();

	setAddress256k(col);
	clr_CAS();

	set_WE();
	set_CAS();
	set_RAS();
	set_data_in();
}

/************************************************************************/
/* ����������� ������ RAS-only, �������� ��� � 8 ��                     */
/************************************************************************/
void Mem256kRegenerate() {
	set_RAS();
	set_CAS();

	uint16_t size = _BV(rows);
	for (uint16_t row = 0 ; row < size; row++) {
		setAddress256k(row);
		clr_RAS();
		nop();
		set_RAS();
	}
}

static uint8_t Mem256kTestPattern(uint8_t pattern) {

	uint8_t errs=0;
	uint16_t size = 1 << rows;
	uint8_t numberPerCell = size >> 3;
	// ��������� ���
	for (uint16_t row = 0; row < size; row++) {
		for (uint16_t col = 0; col < size; col++) {
			Mem256kWriteData(row, col, pattern);
		}
		Mem256kRegenerate();
	}
	set_data256k(0);
	// ���������
	for (uint16_t row = 0; row < size; row++) {
		for (uint16_t col = 0; col < size; col++) {
			uint16_t r = row / numberPerCell;
			uint16_t c = col / numberPerCell;
                        uint16_t curCellPos = r*8+c;
			uint8_t val = Mem256kReadData(row, col);

			if (val != pattern) {
				failures[curCellPos]++;
				errs++;
			}
		}
		Mem256kRegenerate();
	}
	MSG_DEC("TST PATT ", pattern);
	MSG_DEC("Errs ", errs);
	return errs;
}

void Mem256kTest() {
	uint8_t chip_err=0;
	no_chip = 0;


	Mem256kWriteData(0, 0, 0x0f);	Mem256kRegenerate();
	Mem256kWriteData(0, 0, 0x0f);	Mem256kRegenerate();
	Mem256kWriteData(0, 0, 0x0f);	Mem256kRegenerate();
	Mem256kWriteData(5, 5, 0);	Mem256kRegenerate();
	Mem256kWriteData(5, 5, 0);	Mem256kRegenerate();
	Mem256kWriteData(5, 5, 0);	Mem256kRegenerate();

	set_data256k(0x0f);

	uint8_t r1, r2, r3;
	r1 = Mem256kReadData(0, 0);	Mem256kRegenerate();
	r2 = Mem256kReadData(0, 0);	Mem256kRegenerate();
	r3 = Mem256kReadData(0, 0);	Mem256kRegenerate();
	MSG_DEC("TST0 F ", r1);
	MSG_DEC("TST0 F ", r2);
	MSG_DEC("TST0 F ", r3);

	r1 = Mem256kReadData(5, 5);	Mem256kRegenerate();
	r2 = Mem256kReadData(5, 5);	Mem256kRegenerate();
	r3 = Mem256kReadData(5, 5);	Mem256kRegenerate();
	MSG_DEC("TST5 0 ", r1);
	MSG_DEC("TST5 0 ", r2);
	MSG_DEC("TST5 0 ", r3);

	if(r1==r2 && r2==r3 && r3==0x0f)
		chip_err++;

	Mem256kWriteData(0, 0, 0);	Mem256kRegenerate();
	Mem256kWriteData(0, 0, 0);	Mem256kRegenerate();
	Mem256kWriteData(0, 0, 0);	Mem256kRegenerate();
	Mem256kWriteData(5, 5, 0x0f);	Mem256kRegenerate();
	Mem256kWriteData(5, 5, 0x0f);	Mem256kRegenerate();
	Mem256kWriteData(5, 5, 0x0f);	Mem256kRegenerate();

	set_data256k(0x0);
	
	r1 = Mem256kReadData(0, 0);	Mem256kRegenerate();
	r2 = Mem256kReadData(0, 0);	Mem256kRegenerate();
	r3 = Mem256kReadData(0, 0);	Mem256kRegenerate();
	MSG_DEC("TST0 0 ", r1);
	MSG_DEC("TST0 0 ", r2);
	MSG_DEC("TST0 0 ", r3);

	if(r1==r2 && r2==r3 && r3==0x0f)
		chip_err++;

	//_delay_us(10);
	r1 = Mem256kReadData(5, 5);	Mem256kRegenerate();
	r2 = Mem256kReadData(5, 5);	Mem256kRegenerate();
	r3 = Mem256kReadData(5, 5);	Mem256kRegenerate();
	MSG_DEC("TST5 F ", r1);
	MSG_DEC("TST5 F ", r2);
	MSG_DEC("TST5 F ", r3);


	if(chip_err==2)
		no_chip=1;

	if(no_chip) {
		MSG("No Chip");
		return;
	}

	for (uint8_t row = 0; row < 8*8; row++) {
		failures[row] = 0;
	}

	if(Mem256kTestPattern(0x05) > 8*8/2) {
		MSG("test failed");
		return;
	}

	Mem256kTestPattern(0x0a);
#if DEBUG
	Mem256kDebug();
#endif
	MSG("test done");
}

#if DEBUG
void Mem256kDebug() {
	for (uint16_t row = 0; row < 8; row++) {
		for (uint16_t col = 0; col < 8; col++) {
			uart_putdw_dec(failures[row*8+col]);
			uart_putc(' ');
			uart_putc(' ');
		}
		uart_putc('\n');
	}
	uart_putc('\n');
}
#endif


uint8_t Mem256kTestGetCell(uint8_t row, uint8_t col) {
	bool bad = failures[row*8+col] > 0;
	if(no_chip) {
		return TEST_CELL_UNKNOWN;
	} else if (bad) {
		return TEST_CELL_BAD;
	}
	return TEST_CELL_GOOD;
}

