/*
 * memory_test.h
 *
 * Created: 27.07.2015 22:03:34
 *  Author: Trol
 */ 


#ifndef MEMORY_TEST256K_H_
#define MEMORY_TEST256K_H_

/************************************************************************/
/* ���������� �������� �����                                            */
/************************************************************************/
#define TEST_CELL_GOOD		0
#define TEST_CELL_BAD		1
#define TEST_CELL_UNKNOWN	2


#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>


void Mem256kInit();
// ���������� ����������� ����� ��� 0xff ���� �� ������� ���������
uint8_t Mem256kReadData(uint16_t row, uint16_t col);
void Mem256kWriteData(uint16_t row, uint16_t col, uint8_t val);
void Mem256kRegenerate();

void Mem256kTest();
void Mem256kDebug();

/************************************************************************/
/* ���������� TEST_CELL_xxx ��� ����������� ������                      */
/************************************************************************/
uint8_t Mem256kTestGetCell(uint8_t row, uint8_t col);

#endif // MEMORY_TEST256K_H_